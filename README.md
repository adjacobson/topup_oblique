# README #

This will eventually be the directory that contains the test cases for the oblique vs non-oblique topup processing

I ran into an issue when trying to link the data on fmri1 to the repo in bitbucket:

git remote add origin https://adjacobson@bitbucket.org/adjacobson/topup_oblique.git
fatal: Not a git repository (or any parent up to mount point /rpool)
Stopping at filesystem boundary (GIT_DISCOVERY_ACROSS_FILESYSTEM not set).